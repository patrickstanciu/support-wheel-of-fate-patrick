BEGIN;

DROP TABLE IF EXISTS engineers CASCADE;
DROP TABLE IF EXISTS shifts CASCADE;

CREATE TABLE engineers (
  id                    SERIAL,
name                    VARCHAR     PRIMARY KEY     UNIQUE,
  shifts_worked         INTEGER
);

CREATE TABLE shifts (
  id                 SERIAL,
  shift_today        VARCHAR[],
  shift_yesterday    VARCHAR[]
);

INSERT INTO shifts
  (shift_today, shift_yesterday)
VALUES
('{"name1", "name2"}',
'{"name3", "name4"}');


INSERT INTO engineers
  (name, shifts_worked)
VALUES
  ('Stanciu Patrick', 0),
  ('Popescu Ion', 0),
  ('Dan Ban', 0),
  ('Bill Gates', 0),
  ('Tim Cook', 0),
  ('Bestington John', 0),
  ('Testingtion Ion', 0),
  ('Qanalist Mark', 0),
  ('Lacoste Marek', 0),
  ('Johnson Johnson', 0);

COMMIT;
