const getEngineers = require('./database/queries/getEngineers.js');
const getShifts = require('./database/queries/getShifts.js');
const updateEngineers = require('./database/queries/updateEngineers.js');
const updateShifts = require('./database/queries/updateShifts.js');
let bodyParser = require('body-parser')

const express = require("express");

const PORT = process.env.PORT || 3001;
const path = require('path');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.get("/api", (req, res) => {
    res.json({message: "Hello from server!"});
});
app.post('/update-engineers', (req, res) => {
    const engineers = req.body.engineers;
    const shiftToday = req.body.shiftToday;
    const shiftYesterday = req.body.shiftYesterday;

    updateEngineers(engineers, (err, updatedEngineers) => {

        if (err) {
            return res.status(500).json({
                type: 500,
                message: 'error from the server'
            })
        }
        else {
            updateShifts(shiftToday, shiftYesterday, (err, updatedShifts) => {
                if (err) {
                    return res.status(500).json({
                        type: 500,
                        message: 'error from the server'
                    })
                }
                else {
                    res.status(201).json({
                        type: 201,
                        message: 'Post request has been successful',
                        url: '/engineers/update'
                    });
                }
            })
        }
    });
});

app.get('/get-engineers', (req, res) => {
    getEngineers((err, engineers) => {
        if (err) {
            return res.status(500).json({
                type: 500,
                message: 'error from server'
            });
        } else {
            getShifts((err, shifts) => {
                if (err) {
                    return res.status(500).json({
                        type: 500,
                        message: 'error from server'
                    });
                } else {

                    return res.json({
                        engineers: engineers,
                        shiftToday: shifts.shift_today,
                        shiftYesterday: shifts.shift_yesterday
                    })
                }
            })
        }
    });
});

app.use(express.static(path.resolve(__dirname, '../client/build')));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
})
