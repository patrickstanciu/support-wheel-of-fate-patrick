import React from "react";


function EngineersBottom({shiftYesterday}) {

    return (
        <section className="d-block mt-5 flex-column ">
            <div className="d-flex align-items-center justify-content-center">
                <h3 className="text-center p-3"
                    style={{backgroundColor: "rgba(0,0,0,0.3)", width: "30%", borderRadius: ".5em", fontWeight: '700'}}>
                    Last Shifts</h3>
            </div>
            <div className="d-flex align-items-center justify-content-center">
                <p className="col d-flex text-right align-items-center justify-content-end p-3">
                    <span style={{fontSize: "1.25rem", fontWeight: "700"}}>AM &nbsp;</span> {shiftYesterday[0]}
                </p>
                <p className="col p-3 d-flex align-items-center">
                    <span style={{fontSize: "1.25rem", fontWeight: "700"}}>PM &nbsp;</span>{shiftYesterday[1]}
                </p>
            </div>
        </section>
    );
}

export default EngineersBottom;
