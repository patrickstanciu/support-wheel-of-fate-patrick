import React from "react";
import './Header.scss';
import ButtonRectangle from "../ButtonRectangle/ButtonRectangle";

function Header(props) {
    let {title, selectTodaysEngineers} = props
    return (
        <section id="header" className="p-3 d-flex flex-row justify-content-between align-items-center">
            <h4 className="mt-0 ">{title}</h4>
            <ButtonRectangle clicked={()=>selectTodaysEngineers()}
                             text="Pick Engineers"/>
        </section>
    );
}

export default Header;
