import React from "react";
import "./ButtonRectangle.scss"

function ButtonRectangle(props) {
    let {text, clicked} = props
    return (
        <button
            /*onClick={() => selectTodaysEngineers()}*/
            onClick={() => clicked()}
            type="button"
            className="btn btn-secondary button-rectangle"
            style={{fontSize: "1.3rem", fontWeight: "500", backgroundColor: "rgba(0,0,0,0.1)", borderRadius: ".5em"}}>
            {text}
        </button>
    );
}

export default ButtonRectangle;
