import React from "react";
import time from '../../assets/time.svg';
import time2 from '../../assets/time2.svg';

function EngineersSection({shiftToday}) {
    return (
        <section className="flex-row d-flex mt-5">

            <div className="w-50 d-flex flex-column align-items-center">
                <h4 className="pb-4">AM</h4>
                <img
                    className={`w-50 w-40-ns`}
                    src={ time }
                    alt='sunrise illustration'>
                </img>
                <p style={{fontSize:"20px", fontWeight:500}}>{ shiftToday[0] }</p>
            </div>

            <div className="w-50 d-flex flex-column align-items-center">
                <h4 className="pb-4">PM</h4>
                <img
                    className={`w-50 w-40-ns`}
                    src={ time2 }
                    alt='sunrise illustration'>
                </img>
                <p style={{fontSize:"20px", fontWeight:500}}>{ shiftToday[1] }</p>
            </div>

        </section>
    );
}

export default EngineersSection;
