import React from "react";
import "./App.css";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
export default () => (
    <Router>
        <Switch>
            {/* Home */}
            <Route path="/" exact component={Home}/>
        </Switch>
    </Router>
);
