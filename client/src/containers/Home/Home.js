import React, {useEffect, useState, useCallback} from "react";
import './Home.scss';
import Header from "../../components/Header/Header";
import EngineersSection from "../../components/EngineersSection/EngineersSection";
import EngineersBottom from "../../components/EngineersBottom/EngineersBottom";
import filterEligibleEngineers from "../../helpers/filterEligibleEngineers";
import todaysEngineers from "../../helpers/selectTodaysEngineers";
import updateEngineers from "../../requests/updateEngineers";
import getEngineers from "../../requests/getEngineers";


function Home() {
    const [shiftYesterday, setShiftYesterday] = useState([]);
    const [shiftToday, setShiftToday] = useState([]);
    const [engineers, setEngineers] = useState([]);
    useEffect(() => {
        getEngineers.then((data) => {
            setShiftYesterday(data.shiftYesterday)
            setShiftToday(data.shiftToday)
            setEngineers(data.engineers)
        }).catch((err) => {
            // would usually alert the user to the error here
            console.error(err, 'error occured whilst fetching data');
        });
    },[])
    const selectTodaysEngineers =  useCallback(() => {
        let eligibleEngineersList = filterEligibleEngineers(engineers, shiftToday);
        let updatedEngineers = todaysEngineers(eligibleEngineersList, engineers);
        setEngineers(updatedEngineers.engineers)
        setShiftToday(updatedEngineers.shiftToday)
        setShiftYesterday(shiftToday)
        updateEngineers(updatedEngineers.engineers, updatedEngineers.shiftToday, shiftToday);
    }, [shiftToday,engineers]);

    return (
        <div id="home">
            <Header selectTodaysEngineers={ selectTodaysEngineers} title={"SUPPORT WHEEL OF FATE"}/>
            <EngineersSection shiftToday={shiftToday}/>
            { shiftYesterday && shiftYesterday.length > 0 &&
            <EngineersBottom shiftYesterday={shiftYesterday} />
            }
        </div>
    );
}

export default Home;
